package calculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.animation.Shake;

@SuppressWarnings("unused")

public class CalcController {

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonSub;

    @FXML
    private Button buttonMul;

    @FXML
    private Button buttonDiv;

    @FXML
    private Button buttonReset;

    @FXML
    private TextField result;

    @FXML
    void add(ActionEvent event) {
        animation();
        double first = Double.parseDouble(firstOperand.getText());
        double second = Double.parseDouble(secondOperand.getText());
        double sum = first + second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void divide(ActionEvent event) {
        animation();
        double first = Double.parseDouble(firstOperand.getText());
        double second = Double.parseDouble(secondOperand.getText());
        double sum = first / second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void multiply(ActionEvent event) {
        animation();
        double first = Double.parseDouble(firstOperand.getText());
        double second = Double.parseDouble(secondOperand.getText());
        double sum = first * second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void reset(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();
    }

    @FXML
    void subtract(ActionEvent event) {
        animation();
        double first = Double.parseDouble(firstOperand.getText());
        double second = Double.parseDouble(secondOperand.getText());
        double sum = first - second;
        result.setText(String.valueOf(sum));
    }

    private void animation() {
        if (firstOperand.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(firstOperand);
            firstOperandAnim.playAnimation();
        }
        if (secondOperand.getText().isEmpty()) {
            Shake secondOperandAnim = new Shake(secondOperand);
            secondOperandAnim.playAnimation();
        }
        return;
    }
}
