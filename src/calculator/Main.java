package calculator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("calculator.fxml"));
        primaryStage.getIcons().add(new Image("sample\\assets\\icon.png"));
        primaryStage.setTitle("Project v0.02a");
        primaryStage.setScene(new Scene(root, 225, 260));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
