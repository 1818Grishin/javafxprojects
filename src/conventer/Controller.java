package conventer;

import conventer.animation.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private TextField numberField;

    @FXML
    private TextField resultField;

    @FXML
    private ComboBox<?> firstComboBox;

    @FXML
    private ComboBox<?> secondComboBox;

    @FXML
    private Button submitButton;

    @FXML
    private Button resetButton;

    @FXML
    void reset(ActionEvent event) {
        numberField.clear();
        resultField.clear();
    }

    @FXML
    void submit(ActionEvent event) {
        if (numberField.getText().isEmpty()) {
            Shake numberFieldAnim = new Shake(numberField);
            numberFieldAnim.playAnimation();
            return;
        }

        if (firstComboBox.getValue() == null || secondComboBox.getValue() == null) {
            Shake firstComboBoxAnim = new Shake(firstComboBox);
            Shake secondComboBoxAnim = new Shake(secondComboBox);
            firstComboBoxAnim.playAnimation();
            secondComboBoxAnim.playAnimation();
            return;
        }

        Double result = checkForError(numberField);
        if(result == null) {
            return;
        }

        if (firstComboBox.getValue().equals("Сантиметр")) {
            if ("Дециметр".equals(secondComboBox.getValue())) {
                result /= 10;
            } else if ("Метр".equals(secondComboBox.getValue())) {
                result /= 100;
            } else if ("Километр".equals(secondComboBox.getValue())) {
                result /= 100000;
            }
        }

        if (firstComboBox.getValue().equals("Дециметр")) {
            if ("Сантиметр".equals(secondComboBox.getValue())) {
                result *= 10;
            } else if ("Метр".equals(secondComboBox.getValue())) {
                result /= 10;
            } else if ("Километр".equals(secondComboBox.getValue())) {
                result /= 10000;
            }
        }

        if (firstComboBox.getValue().equals("Метр")) {
            if ("Сантиметр".equals(secondComboBox.getValue())) {
                result *= 100;
            } else if ("Дециметр".equals(secondComboBox.getValue())) {
                result *= 10;
            } else if ("Километр".equals(secondComboBox.getValue())) {
                result /= 1000;
            }
        }

        if (firstComboBox.getValue().equals("Километр")) {
            if ("Сантиметр".equals(secondComboBox.getValue())) {
                result *= 100000;
            } else if ("Дециметр".equals(secondComboBox.getValue())) {
                result *= 10000;
            } else if ("Метр".equals(secondComboBox.getValue())) {
                result *= 1000;
            }
        }

        resultField.setText(String.valueOf(result));
    }

    private Double checkForError(TextField operand) {
        try {
            return Double.parseDouble(operand.getText());
        } catch (NumberFormatException e) {
            resultField.setText("Ошибка");
        }
        return null;
    }
}
