package conventer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("conventer.fxml"));
        primaryStage.getIcons().add(new Image("conventer\\assets\\icon.png"));
        primaryStage.setTitle("Project 0.03a");
        primaryStage.setScene(new Scene(root, 400, 260));
        primaryStage.show();
        primaryStage.setResizable(false);
    }

    public static void main(String[] args) {
        launch(args);
    }
}