package database;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignUpController {

    @FXML
    private PasswordField signUpPassword;

    @FXML
    private TextField signUpLogin;

    @FXML
    private Button signUpLoginButton;

    @FXML
    private TextField signUpSurname;

    @FXML
    private TextField signUpName;

    @FXML
    private TextField signUpCountry;

    @FXML
    private CheckBox checkBoxSignUpMale;

    @FXML
    private CheckBox checkBoxSignUpFemale;

    @FXML
    void initialize() {

        signUpLoginButton.setOnAction(event -> {
            signUpUser();
        });
    }

    private void signUpUser() {
        DatabaseHandler dbHandler = new DatabaseHandler();

        String firstName = signUpName.getText();
        String surname = signUpSurname.getText();
        String login = signUpLogin.getText();
        String password = signUpPassword.getText();
        String location = signUpCountry.getText();
        String gender = "";

        if(checkBoxSignUpMale.isSelected()) {
            gender = "Male";
        } else {
            gender = "Female";
        }

        User user = new User(firstName, surname, login, password, location, gender);

        dbHandler.signUpUser(user);
    }
}
