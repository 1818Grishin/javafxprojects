package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.animation.Shake;

public class Controller {

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClear;

    @FXML
    private TextField additionResult;

    @FXML
    void add(ActionEvent event) {
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(firstOperand);
            Shake secondOperandAnim = new Shake(secondOperand);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            return;
        }
        int first = Integer.parseInt(firstOperand.getText());
        int second = Integer.parseInt(secondOperand.getText());
        int sum = first + second;
        additionResult.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        additionResult.clear();
    }

}
