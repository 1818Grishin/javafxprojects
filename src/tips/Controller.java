package tips;

import com.jfoenix.controls.JFXToggleButton;
import tips.animation.Shake;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import java.text.DecimalFormat;

public class Controller {

    @FXML
    private TextField tipsField;

    @FXML
    private TextField totalField;

    @FXML
    private TextField rubBill;

    @FXML
    private TextField rubTips;

    @FXML
    private TextField rubTotalField;

    @FXML
    private Label rubBillLabel;

    @FXML
    private Label rubTipsLabel;

    @FXML
    private Label rubTotalLabel;

    @FXML
    private Button submitButton;

    @FXML
    private Button clearButton;

    @FXML
    private TextField billInputField;

    @FXML
    private ComboBox<?> comboBox;

    @FXML
    private Slider slider;

    @FXML
    private JFXToggleButton toggleRub;

    @FXML
    private JFXToggleButton toggleRound;

    @FXML
    private Label errorLabel;

    /**
     * Очищает все поля и скрывает все ошибки при нажатии на кнопку "Очистить"
     */
    @FXML
    void clear() {
        tipsField.clear();
        totalField.clear();
        rubBill.clear();
        rubTips.clear();
        rubTotalField.clear();
        billInputField.clear();
        toggleRub.setVisible(false);
        switchElementsVisible(false);
        errorLabel.setVisible(false);
    }

    /**
     * Вычисляет все значения чека при нажатии на кнопку "Вычислить"
     */
    @FXML
    void submit() {
        errorLabel.setVisible(false);
        checkForNeededData();
        Double billAmount = getInputData(billInputField);
        checkForInputError();
        if (billAmount == null || billAmount <= 0) {
            return;
        }
        double tips, totalBill;
        double tipsPercent = slider.getValue() * 0.01;

        if(comboBox.getValue().equals("Рубль")) {
            toggleRub.setVisible(false);
            switchElementsVisible(false);

            tips = billAmount * tipsPercent;
            totalBill = billAmount + tips;

            if (!toggleRound.isSelected()) {
                tipsField.setText(String.valueOf(formatToDecimal(tips)));
                totalField.setText(String.valueOf(formatToDecimal(totalBill)));
            } else {
                tipsField.setText(String.valueOf(formatToFloorRound(tips)));
                totalField.setText(String.valueOf(formatToCeilRound(totalBill)));
            }
        } else if(comboBox.getValue().equals("Доллар")) {
            toggleRub.setVisible(true);
            tips = billAmount * tipsPercent;
            totalBill = billAmount + tips;

            if (!toggleRound.isSelected()) {
                tipsField.setText(String.valueOf(formatToDecimal(tips)));
                totalField.setText(String.valueOf(formatToDecimal(totalBill)));

                rubBill.setText(String.valueOf(formatToDecimal(billAmount * 73)));
                rubTips.setText(String.valueOf(formatToDecimal(tips * 73)));
                rubTotalField.setText(String.valueOf(formatToDecimal(totalBill * 73)));

            } else {
                tipsField.setText(String.valueOf(formatToFloorRound(tips)));
                totalField.setText(String.valueOf(formatToCeilRound(totalBill)));

                rubBill.setText(String.valueOf(formatToFloorRound(billAmount * 73)));
                rubTips.setText(String.valueOf(formatToFloorRound(tips * 73)));
                rubTotalField.setText(String.valueOf(formatToCeilRound(totalBill * 73)));

            }
        } else if (comboBox.getValue().equals("Евро")){
            toggleRub.setVisible(true);
            tips = billAmount * tipsPercent;
            totalBill = billAmount + tips;

            if (!toggleRound.isSelected()) {
                tipsField.setText(String.valueOf(formatToDecimal(tips)));
                totalField.setText(String.valueOf(formatToDecimal(totalBill)));

                rubBill.setText(String.valueOf(formatToDecimal(billAmount * 78)));
                rubTips.setText(String.valueOf(formatToDecimal(tips * 78)));
                rubTotalField.setText(String.valueOf(formatToDecimal(totalBill * 78)));

            } else {
                tipsField.setText(String.valueOf(formatToFloorRound(tips)));
                totalField.setText(String.valueOf(formatToCeilRound(totalBill)));

                rubBill.setText(String.valueOf(formatToFloorRound(billAmount * 78)));
                rubTips.setText(String.valueOf(formatToFloorRound(tips * 78)));
                rubTotalField.setText(String.valueOf(formatToCeilRound(totalBill * 78)));
            }
        }
    }

    /**
     * Считывает положение переключателя отображения полей со значениями чека в руб.
     */
    @FXML
    void showRubFields() {
        if (toggleRub.isSelected()) {
            switchElementsVisible(true);
        } else {
            switchElementsVisible(false);
        }
    }

    /**
     * Переключает отображение полей чека в рублях
     * @param toggle вкл/выкл отображения полей
     */
    private void switchElementsVisible(boolean toggle) {
        rubTipsLabel.setVisible(toggle);
        rubTotalLabel.setVisible(toggle);
        rubBillLabel.setVisible(toggle);
        rubBill.setVisible(toggle);
        rubTips.setVisible(toggle);
        rubTotalField.setVisible(toggle);
    }

    /**
     * Принимает значение из поля ввода для послед. использования в вычислениях
     *
     * @param operand из какого поля будут считываться значения
     * @return числовое значение (null при вводе нечисловых данных)
     */
    private Double getInputData(TextField operand) {
        try {
            String inputData = operand.getText();
            inputData = inputData.replaceAll(",", ".");
            double toOutput = Double.parseDouble(inputData);
            return toOutput;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Проверяет полученное значение и в зависимости от результата выводит ошибку
     */
    private void checkForInputError() {
        if (getInputData(billInputField) == null || getInputData(billInputField) <= 0) {
            errorLabel.setVisible(true);
        }
    }

    /**
     * Проверяет наличие данных и их корректность
     * При отсутствии данных или нечисловом формате выдаёт ошибку и производит анимацию
     */
    private void checkForNeededData() {
        if (billInputField.getText().isEmpty() || billInputField.getText().equals("\\d")) {
            Shake numberFieldAnim = new Shake(billInputField);
            numberFieldAnim.playAnimation();
            return;
        }
    }

    /**
     * Форматирует число для отображения сотых значений
     *
     * @param number число для преобразования
     * @return форматированное число
     */
    private String formatToDecimal(double number) {
        return new DecimalFormat("#.##").format(number);
    }
                                                                                                                                                                                                                                                                   //я все ещё не знаю как сделать корректное отображение чисел

    /**
     * Округляет до целого числа вниз
     *
     * @param number число для преобразования
     * @return форматированное число
     */
    private String formatToFloorRound(double number) {
        number = Math.floor(number);
        return new DecimalFormat("#.00").format(number);
    }

    /**
     * Округляет до целого числа вверх
     *
     * @param number число для преобразования
     * @return форматированное число
     */
    private String formatToCeilRound(double number) {
        number = Math.ceil(number);
        return new DecimalFormat("#.00").format(number);
    }
}