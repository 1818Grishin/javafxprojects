package tips;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("tips.fxml"));
       // primaryStage.getIcons().add(new Image("tips\\assets\\icon.png"));
        primaryStage.setTitle("Tips Calculator");
        primaryStage.setScene(new Scene(root, 350, 290));
        primaryStage.show();
        primaryStage.setResizable(false);
    }

    public static void main(String[] args) {
        launch(args);
    }
}